#include <bits/stdc++.h>
#include <sys/times.h>
#include <unistd.h>
#include <time.h>

using namespace std;

int main() {
	freopen("data", "r", stdin);
	int n;
	cin >> n;
	vector<int> values(n);
	for (int i = 0; i < n; i++) {
		cin >> values[i];
	}

	struct tms start_times, end_times;
	long clocks_per_sec = sysconf(_SC_CLK_TCK);
	long clocks;
	struct timespec start_cgt, end_cgt;

	clock_gettime(CLOCK_MONOTONIC_RAW, &start_cgt);
	times(&start_times);

	for (int j = 0; j < n; j++) {
		for (int i = 0; i < n - 1; i++) {
			if (values[i] > values[i + 1])
				swap(values[i], values[i + 1]);
		}
	}
	clock_gettime(CLOCK_MONOTONIC_RAW, &end_cgt);
	times(&end_times);

	printf("[clock_gettime] Time taken: %lf sec\n", end_cgt.tv_sec-start_cgt.tv_sec + 0.000000001*(end_cgt.tv_nsec-start_cgt.tv_nsec));
	clocks = end_times.tms_utime - start_times.tms_utime;
	printf("[times] Time taken: %lf sec\n\n", (double)clocks / clocks_per_sec);

}
