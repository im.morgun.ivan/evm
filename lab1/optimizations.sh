
g++ taskcode/generator.cpp -o generator.out
g++ -O0 taskcode/main.cpp -o main_O0.out
g++ -O1 taskcode/main.cpp -o main_O1.out
g++ -O2 taskcode/main.cpp -o main_O2.out
g++ -O3 taskcode/main.cpp -o main_O3.out
g++ -Os taskcode/main.cpp -o main_Os.out
g++ -Ofast taskcode/main.cpp -o main_Ofast.out
g++ -Og taskcode/main.cpp -o main_Og.out

./generator.out 10000
for ((k=0;k<20;k=k+1))
do
	./main_O0.out
done

for ((j=100000; j<1500000; j=j+25000))
do

./generator.out $j;
echo $j;

echo "====================Running O0 optimization====================" >> log
for i in $(seq 1 $1);
do
	./main_O0.out >> log;
done

echo "====================Running O1 optimization====================" >> log
for i in $(seq 1 $1);
do
        ./main_O1.out >> log;
done

echo "====================Running O2 optimization====================" >> log
for i in $(seq 1 $1);
do
        ./main_O2.out >> log;
done


echo "====================Running O3 optimization====================" >> log
for i in $(seq 1 $1);
do
        ./main_O3.out >> log;
done

echo "====================Running Os optimization====================" >> log
for i in $(seq 1 $1);
do
        ./main_Os.out >> log;
done

echo "====================Running Ofast optimization====================" >> log
for i in $(seq 1 $1);
do
        ./main_Ofast.out >> log;
done

echo "====================Running Og optimization====================" >> log
for i in $(seq 1 $1);
do
        ./main_Og.out >> log;
done
done

